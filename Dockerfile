FROM registry.vecps.com/default/golang:1.17.7-alpine3.15

LABEL author=hexiaolong.hexl@bytedance.com

COPY welcome.go .
RUN go build ./welcome.go
#COPY welcome .

EXPOSE 80

ENTRYPOINT ["./welcome"]
